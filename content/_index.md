---
title: "welcome"
date: 2020-09-20T15:00:35+01:00
draft: false
---
# Welcome to the self-taught engineer.
My name is Tim Frazer. I've worked at a wearable tech startup [PAI Health](https://www.paihealth.com), a retail startup & eCommerce
[Kit & Ace](https://www.kitandace.com) and now reside in New York as a Senior Engineer at [Bonobos](https://www.bonobos.com) on the Data engineering team.

I've worked many jobs in my life but being an engineer is the one I've loved.

This road was hard and still is demanding but rewarding. I practice what I post and would never post something I haven't been doing myself.

I'm a forever learner and have no plans on stopping - this space is where I will write about and teach at times how to learn technology fast, make mistakes and iterate the career ladder of being a self-taught engineer.

*Tim Frazer, Brooklyn, New York*
