---
title: "About"
#date: 2021-04-22T07:50:29-04:00
draft: false
include_toc: false
---
# Bio
![Tim Frazer photo](/img/tim.png)
___
Tim Frazer has been a professional software engineer at [Kit & Ace](https://www.kitandace.com/),and currently [Bonobos](http://bonobos.com/)  part of [Walmart eCommerce](https://corporate.walmart.com/newsroom/2017/06/16/walmart-to-acquire-bonobos-and-appoint-andy-dunn-to-oversee-exclusive-consumer-brands-offered-online). He grew up on a small island off the coast of Vancouver, Canada. Naturally being homeschooled by hippy parents, he travelled the world a few times spending long amounts of time in India, Indonesia, New Zealand, Japan, England before settling down in Brooklyn, New York with his wife [Christina](http://wayman.xyz/), and dog Skye.

*Previous stints have been -*

Organic farmer, yoga teacher, retail buyer, operations manager, hobby chef, business analyst and several other roles he has forgotten as he got older.
