---
title: "Markdown"
#date: 2021-05-12T07:26:03-04:00
draft: false
---
# Quick Sheet: Markdown

### Heading
```
# H1
## H2
### H3
```
# H1
## H2
### H3

### Bold
```
**bold text**
```
**bold text**
### Italic
```
*italicized text*
```
*italicized text*
### Blockquote
```
> blockquote
```
> blockquote
### Ordered List
```
1. First item
2. Second item
3. Third item
```
1. First item
2. Second item
3. Third item
### Unordered List
```
- First item
- Second item
- Third item
```
- First item
- Second item
- Third item
### Code
```
`code`
```
### Horizontal Rule
```
---
```
---
### Link
```
[title](https://www.example.com)
```
[title](https://www.example.com)
### Image
```
![alt text](image.jpg)
```
![alt text](../img/logo.png)
## Extended Syntax

These elements extend the basic syntax by adding additional features. Not all Markdown applications support these elements.

### Table

| Syntax | Description |
| ----------- | ----------- |
| Header | Title |
| Paragraph | Text |

```

| Syntax | Description |
| ----------- | ----------- |
| Header | Title |
| Paragraph | Text |

```

### Fenced Code Block
Use triple `

```
{
  "firstName": "John"
}
```
### Github Flavoured Syntax
`Just add ```java CODE BLOCK ``` `
```javascript
function fancyAlert(arg) {
  if(arg) {
    $.facebox({div:'#foo'})
  }
}
```
### Footnote
```
Here's a sentence with a footnote. [^1]
[^1]: This is the footnote.
```
Here's a sentence with a footnote. [^1]
[^1]: This is the footnote.
### Heading ID
```
### My Great Heading {#custom-id}
```
### My Great Heading {#custom-id}
### Definition List
```
term
: definition
```
term
: definition
### Strikethrough
```
~~The world is self-taught.~~
```
~~The world is self-taught.~~
### Task List

```
- [x] Write the press release
- [ ] Update the self-taught
- [ ] Post to twitter

```

Ref [GitHub Markdown](https://guides.github.com/features/mastering-markdown/)

Ref [Emoji Cheat Sheet](https://github.com/ikatyang/emoji-cheat-sheet)

Ref [Mark Down Guide](https://www.markdownguide.org/)