---
title: "Pytest"
draft: false
---
# Quick Sheet: Pytest

### Conftest 

### Verbose output
`pytest test_sample.py -v`  
### Run specific test
`pytest `test_server.py::TestClass::test_method`
### Use PDB after 1st fail
`py.test -x --pdb`

### Example Fixture tmp_path for files
```python
# content of test_tmp_path.py
CONTENT = "content"

def test_create_file(tmp_path):
    d = tmp_path / "sub"
    d.mkdir()
    p = d / "hello.txt"
    p.write_text(CONTENT)
    assert p.read_text() == CONTENT
    assert len(list(tmp_path.iterdir())) == 1
    assert 0

```
### Example paramertize
```python
@pytest.mark.parametrize(('n', 'expected'), [(1, 2)])
def test_increment(n, expected):
    assert n == expected
```
### Example Mock

### Example monkeypatch

```python
import pytest
from function import square

def test_main_function(monkeypatch): 
    monkeypatch.setattr('function.square', lambda x: 1) 
    assert square(5) == 1
```

### raises - test for failure
```python
import pytest

def square():
    raise RuntimeError
        

def test_assert():
    with pytest.raises(RuntimeError) as e:
        square()
    assert 'something' in str(e.value)
```

### Drop to Python debugger on 1st failure
`py.test -x --pdb `