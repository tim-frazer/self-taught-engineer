---
title: "Bob Belderbos June 10, 2021"
date: 2021-06-10T9:45:57-04:00
draft: false
include_toc: true
---
# June 10th, 2021 PDM Questions

## Questions
1. What skills should I be focusing on for becoming a senior developer - it's becoming quite clear to me that out of where I currently work I am an intermediate developer at best, and I would like to focus on a few key areas this year what do you think they should be?

1a.
    Work together on PDM with other people.  Solve more problems.
----

2. After several rounds of interviews, I failed to land 2 interviews that I thought I would get offers from input I got was that I was weaker than they thought I would be in my technical output.
------

------
### comment 

3. Areas I seem to struggle with complex iteration and writng code quickly without having to look stuff up and laying out the problem I need to solve - thoughts?

4. Started writing an article for testdriven.io - this is a super draft - but what do you think of the idea?
https://selftaught.engineer/articles/serverless-python-api/
https://aws.amazon.com/athena/
https://aws.amazon.com/auora/
### comment

5. Coding challenge
This is a coding challenge I was given, and had 30 minutes to solve.
I really started to struggle as I broke down the problem 
## comment
---
recurision & regular expression would have solved this problem
----
### Code Challenge

How might you solve this?
Let's pair code 

```python
###
#   In this exercise we will take a dict with string keys and transform them to be snake_case.
#   the `raw` input will be something of the format:
#   ```{
#         "camelCase": "Value1",
#         "camelcase": "Value1",
#         "PascalCase": "Value2",
#         "kebab-case": "Value3",
#         "ACRONYM": "Value4",
#         "number22": "Value5"
#      }
#   ```
#   and the result should make the keys snake case, this includes handling hyphens, numbers, acronyms,
#   and nested object/arrays. In this simple example
#   the expected output is
#   ```{
#         "camel_case": "Value1",
#.        "camelcase": "Value1",
#         "pascal_case": "Value2",
#         "kebab_case": "Value3",
#         "a_c_r_o_n_y_m": "Value4",
#         "number_22": "Value5"
#      }
#   ```
###

runTests = True
stopAfterFirstFailure = False


# Tim's Code 
def solution(raw: dict) -> dict:
    keys = raw.keys()
    newRaw = {}
    newString=''
    
    for key in keys:
        for index, char in enumerate(key):
            if char.isupper() and index != 0:
                newString += '_' + char.lower()
            elif char.isnumeric():
                newString += '_'+ char.lower()
            elif char.isalpha() == False: #executeOrder66
                newString += '_'
            else:
                newString += char.lower()
                
            
        newRaw[newString] = (raw.get(key))
   
    return newRaw 
    
    
    


import pytest


def test_empty_object():
    assert {} == solution({})


def test_no_transform():
    input = {'star': 'wars'}
    assert {'star': 'wars'} == solution(input)


def test_single_key_transform():
    input = {'starWars': 'is awesome'}
    assert {'star_wars': 'is awesome'} == solution(input)


def test_leading_capital():
    input = {'DarthVader': 'James Earl Jones'}
    assert {'darth_vader': 'James Earl Jones'} == solution(input)


def test_snake_case_stays():
    input = {'darth_vader': 'James Earl Jones'}
    assert input == solution(input)


def test_transform_kebab_case():
    input = {'darth-vader': 'James Earl Jones'}
    assert {'darth_vader': 'James Earl Jones'} == solution(input)


def test_acronyms():
    input = {'numTIEFighters': 1e5}
    assert {'num_t_i_e_fighters': 1e5} == solution(input)


def test_single_digit():
    input = {'emperorsNumber2': 'Darth Vader'}
    assert {'emperors_number_2': 'Darth Vader'} == solution(input)


def test_multi_digit():
    input = {'executeOrder66': 'yes sir!'}
    assert {'execute_order_66': 'yes sir!'} == solution(input)


def test_multi_first_level_keys():
    input = {'firstName': 'Han', 'lastName': 'Solo'}
    assert {'first_name': 'Han', 'last_name': 'Solo'} == solution(input)


def test_nested_object():
    input = {
        'darthVader': {
            'firstName': 'Anakin',
            'lastName': 'Skywalker',
            'appearance': {
                'helmetColor': 'black',
                'armorColor': 'black',
                'capeColor': 'black'
            }
        }
    }
    expected = {
        'darth_vader': {
            'first_name': 'Anakin',
            'last_name': 'Skywalker',
            'appearance': {
                'helmet_color': 'black',
                'armor_color': 'black',
                'cape_color': 'black'
            }
        }
    }
    assert expected == solution(input)


def test_nested_array():
    input = {
        'firstName': 'Anakin',
        'lastName': 'Skywalker',
        'children': [
            {'firstName': 'Luke'},
            {'firstName': 'Leia'}
        ]
    }

    expected = {
        'first_name': 'Anakin',
        'last_name': 'Skywalker',
        'children': [
            {'first_name': 'Luke'},
            {'first_name': 'Leia'}
        ]
    }
    assert expected == solution(input)


def test_arrays_in_arrays():
    input = {
        'random': [
            'Luke',
            [
                'blowing up the death star',
                {
                    'skillName': 'bulls-eye womprats',
                    'skillParameters': 'with my T47'
                }
            ]
        ]
    }

    expected = {
        'random': [
            'Luke',
             [
                 'blowing up the death star',
                 {
                     'skill_name': 'bulls-eye womprats',
                     'skill_parameters': 'with my T47'
                 }
             ]
        ]
    }
    assert expected == solution(input)


if runTests:
    opts = []

    if stopAfterFirstFailure:
        opts.append('--maxfail=1')

    pytest.main(opts)
else:
    print("Test running is disabled, set `runTests` to `True` to see test results.")
```