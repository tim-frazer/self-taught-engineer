---
title: "Bob Belderbos June 22nd, 2021"
date: 2021-06-22T9:18:57-04:00
draft: false
include_toc: true
---
# June 22nd, 2021 PDM Questions

## Questions

Let's solve this together and review my mistakes - time limit was 30 minutes.
https://github.com/Timfrazer/try_git/blob/master/Ribbon_Data_Eng__Tim_Frazer.ipynb


1.  Proven patterns to ingest data from URLS
2.  review my draft & and areas for improvement [serverless-python-api](https://selftaught.engineer/articles/serverless-python-api/)
or check out the repo https://github.com/Timfrazer/fastapi-serverless-data