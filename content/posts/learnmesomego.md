---
title: "Learn Go in X time"
date: 2021-06-10T07:45:57-04:00
draft: true
include_toc: false
---

Recently, while looking at rising tehcnical tide in the backend engineering world I've noticed serveral languages that have been circulating a lot on job postings and growing. I currently program mostly in Python, which I love coding in but I think you should try and learn something new once you've become comfortable in 1 language. By learning more about other languages you can become better at the one you prefer also.

I've decided to learn Go lang as it is in the top 10 languages, it's in production at many companies I respect and looks like fun. 

I'll admit I was super tempted to try Rust beyond the 5 minutes I have spent poking around and I might in the near future but I felt like this would.

https://learnxinyminutes.com/docs/go/


[Stackoverflow's 2020 most popular technologies)](https://insights.stackoverflow.com/survey/2020#most-popular-technologies)