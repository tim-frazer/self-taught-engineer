---
title: "Learn Ruby in Y time"
date: 2021-05-18T07:45:57-04:00
draft: true
---

I had the opportunity to dip my toes into some ruby projects recently, and I haven't touched the language in a long time.
Two ~ years beyond a few glances here and there, I thought it was time to do a dramatic refresher. 
However, if you look at Linkedin and many of the eCommerce companies today ( Bonobos, Casper, ) they all still use Ruby. Also, Shopify is mainly built using ruby, so I think it is still valuable to have a pretty good grasp. Now, I already program in Python, so there should be many crossovers, but as I said, it's been over two years since I've had to use the language.

I'll admit I was super tempted to try Rust, Go, Elixir or something newer beyond the 5 minutes I have spent poking around, and I might add soon, but I felt like this would method of re-learning would be valuable.

Mastering - now I want to be pretty efficient at this and the limited amount of time I have.
I'm going to set a goal of 10 hours of active re-learning with an expectation of being able to build out three progressively smaller projects with tests.

I'm going to outline a few goals and methods I'm going to use to learn Ruby + Ruby on Rails in Y time - 

My only real caveat - the ruby community, to me _seems_ to be slowly dying, which is too bad because of what I remember it being a super productive language, especially with web development. Still, you do any actual searches on google, and you will get a pretty depressing pile of links that haven't been updated since the year 201x hell some of the best books on testing haven't been updated for ages. [Effective Testing with RSpec 3](https://pragprog.com/titles/rspec3/effective-testing-with-rspec-3/)

However, I believe this is not the whole story that Ruby or Ruby on Rails is "dying". It only means that the peak of Ruby on Rails's popularity has passed, and the period of maturity has come just like many languages before.
Most iterations are happening, just nothing dramatic, although ruby 3 and several developments that Shopify is making could significantly improve speed, type hints etc.

## Goals
There are a few things I'm going to build to prove to myself that I've learned this stuff and I think building "real" things is the fastest way to learn.

1. CLI tool that takes an input and does a thing
2. Web API - 
3. Web Application - Fake twitter

## Resources 
**note** 

I never go cheap on learning materials, I think it is the single best thing you can invest in is yourself.
I have memberships with o'reilly, pluralsight, skillshare, and several other sites as I think it's pretty important investment and supports the authors. However, I also use youtube a fair bit.

[Learn X in Y minutes](https://learnxinyminutes.com/docs/ruby/)

[Pluralsight](https://app.pluralsight.com/paths/skills/ruby-language-fundamentals)

[Pragmatic Studio Ruby](https://pragmaticstudio.com/courses/ruby)

[Pragmatic Studio Rails](https://pragmaticstudio.com/rails)

[Effective Testing with RSpec 3](https://pragprog.com/titles/rspec3/effective-testing-with-rspec-3/)

[Stackoverflow's 2020 most popular technologies)](https://insights.stackoverflow.com/survey/2020#most-popular-technologies)