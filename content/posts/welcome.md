---
title: "Welcome"
date: 2021-04-22T07:41:37-04:00
draft: false 
---
## Welcome to the self-taught engineer.
I'm Tim Frazer, a Self Taught engineer. I've worked at a wearable tech startup www.paihealth.com, a retail startup www.kitandace.com and now reside in New York as a Senior Engineer at www.bonobos.com on the Data engineering team.

I've worked many jobs in my life but being an engineer is the one I've loved. 

This road was hard and still is demanding but rewarding. 

I'm a forever learner and have no plans on stopping - this space is where I will write about and teach at times how to learn technology fast, make mistakes and iterate the career ladder of being a self-taught engineer.
