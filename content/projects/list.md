---
title: "Projects & due dates"
date: 2021-05-18T07:45:57-04:00
draft: false
---
# Overview
This is my general list of articles and projects


## Python projects

### Serverless + FastAPI + Redshift

How to create a serverless API that handles basic data transformation and validation before writing to redshift.
using Test driven development methods and pytest

[] draft table of contents

### Serverless + FastAPI + Aurora + Twillio

How to create a small quiz game that manages questions via twillio and stores questions and answers in a serverless database.

[] draft table of contents
----
*Resources*

https://docs.aws.amazon.com/AmazonRDS/latest/AuroraUserGuide/UsingWithRDS.IAMDBAuth.Connecting.Python.html
https://pypi.org/project/aurora-data-api/

## Ruby projects

[Learn me some ruby](https://selftaught.engineer/posts/learnmesomeruby/)


